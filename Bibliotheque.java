public class Bibliotheque
{
    public static void main(String[] args)
    {
        Livre oui = new Livre();
        oui.id = 1;
        oui.titre = "Oui-Oui joue au football";
        oui.isbn = 123;
        oui.auteur = "Anne Machand Kalicky";
        oui.prix = 0.75;

        Auteur amk = new Auteur();
        amk.id = 1;
        amk.prenom = "Anne Marchand";
        amk.nom = "Kalicky";
        amk.adresse = "2 rue de la paix";

        Adresse adr = new Adresse();
        adr.id = 1;
        adr.avenue = "rue de la paix";
        adr.ville = "Toulouse";
        adr.pays = "France";

        System.out.println("Id : " + oui.id + "\n" + 
                           "Titre : " + oui.titre + "\n" +
                           "Prix : " + oui.prix + " euros" +"\n" + 
                           "Auteur : " + oui.auteur + "\n" +
                           "Adresse : " + amk.adresse);

    }
}